from flarecast.service.flarecast_service import FlarecastService
from jsonschema import draft4_format_checker

# create flarecast service
service = FlarecastService('Staging Service')
service.create(port=8005,
               specification_dir='stagingservice/swagger/')

# add yaml files
service.app.add_api('stagingservice.yaml', validate_responses=True)

# publish uwsgi flask app variable
application = service.app.app

if __name__ == '__main__':
    service.run()
