# Staging Service
## API Reference
### <font color='blue'>[GET]</font> /swpc_srs/list
Gets all SWPC SRS entries.

### <font color='blue'>[GET]</font> /swpc_event/list
Gets all SWPC EVENT entries.

Filters can be applied using query parameters (see Swagger API).

### <font color='blue'>[GET]</font> /gvloc/list
Gets all SWPC SRS entries.

Filters can be applied using query parameters (see Swagger API).

### <font color='blue'>[GET]</font> /configset/{dataset}/list
Gets configuration sets with their configurations and predictions.

Filters can be applied using query parameters (see Swagger API).

<br />

## Examples
- Tutorial how to retrieve data from the staging service with python and requests:
[Ingest staging data in database](https://dev.flarecast.eu/confluence/pages/viewpage.action?pageId=10944624)

