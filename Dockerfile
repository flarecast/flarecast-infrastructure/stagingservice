FROM flarecast/python-base
MAINTAINER i4Ds

LABEL eu.flarecast.type="system" \
      eu.flarecast.name="stagingservice"

CMD ["uwsgi", "--http", ":8005", "-w", "main", "-p", "16"]

WORKDIR /service/

COPY ./main.py /service/main.py
COPY ./stagingservice /service/stagingservice
