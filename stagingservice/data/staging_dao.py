import uuid
from stagingservice import config
import psycopg2
from psycopg2.extras import RealDictCursor
from psycopg2.extras import Json
from psycopg2.extensions import AsIs
from psycopg2.extensions import register_adapter

from flarecast.service.db_connection import get_db

from sirql import SiRQLToSql


class StagingDAO(object):
    """
    Data access object for connecting to the 'staging' schema of a postgres
    database. StagingDAO is a wrapper class which uses psycopg2 as database
    adapter. Example using the "which" statement (recommended):

        with StagingDAO(auto_commit=False) as ctx:
            swpc_srs_entries = ctx.get_swpc_srs_entries(
                ['*'], {'nar': '2472'}
            )
    """
    def __init__(self, auto_commit=True):
        """
        Creates a new StagingDAO.
        The parameter auto_commit defines if a commit is send to the database
        whenever an update or insert method is called. If auto_commit is set
        to 'False' the StagingDAO.commit() method has to be called manually
        at the end of a transaction.

        :param auto_commit: enables/disables auto commits (default=True)
        """
        self.__con = get_db(manual_handling=True)
        self.__con.cursor_factory = RealDictCursor
        self.__con.autocommit = auto_commit
        self.__cur = self.__con.cursor()

        psycopg2.extensions.register_adapter(dict, psycopg2.extras.Json)

    def __enter__(self):
        """
        Method which is called when entering a "which" statement.

        :return: self
        """
        return self

    def __exit__(self, exception_type, exception_value, traceback):
        """
        Method which is called when leaving a "which" statement.
        Hereby, the cursor as well as the database connection
        will be closed.
        """
        self.__con.rollback()
        self.__cur.close()
        self.__con.close()

    @property
    def connection(self):
        """
        Method to obtain the (raw) database connection.

        :return: database connection
        """
        return self.__con

    @property
    def cursor(self):
        """
        Method to obtain the (raw) database cursor.

        :return: database cursor
        """
        return self.__cur

    # --- public ---

    # workflow
    def commit(self):
        """
        Method which commits a pending database transaction.
        """
        self.__con.commit()

    # get
    def get_swpc_srs_entries(self, fields=["*"], where={}):
        """
        Method which gets a list of SWPC SRS entries.

        :param fields: list of field names
        :param where: dictionary of conditions
        :return: RealDictCursor to the entities
        """
        return self._get(config.SWPC_SRS_TABLE, fields, where)

    def get_swpc_event_entries(self, fields=["*"], where={}):
        """
        Method which gets a list of SWPC EVENT entries.

        :param fields: list of field names
        :param where: dictionary of conditions
        :return: RealDictCursor to the entities
        """
        return self._get(config.SWPC_EVENT_TABLE, fields, where)

    def get_gevloc_entries(self, fields=["*"], where={}):
        """
        Method which gets a list of GEVLOC entries.

        :param fields: list of field names
        :param where: dictionary of conditions
        :return: RealDictCursor to the entities
        """
        return self._get(config.GEVLOC_TABLE, fields, where)

    def _get(self, table, fields=["*"], where={}):
        """
        Method which gets a list of entities.

        :param table: the table to get entities from
        :param fields: list of field names
        :param where: dictionary of conditions
        :return: RealDictCursor to the entities
        """
        self.__cur.execute(
            "SELECT " + ",".join("%s" for f in fields) + " " +
            "FROM " + table + " " +
            "WHERE True " + "".join("AND %s=%s " for k in where.keys()),
            (
                tuple([AsIs(f) for f in fields]) +
                sum([(AsIs(key), where[key]) for key in where.keys()], ())
            )
        )
        return self.__cur.fetchall()

    # get by sirql
    def get_swpc_srs_entries_by_sirql(
        self, fields=["*"], sirql_conditions=[]
    ):
        """
        Method which gets a list of SWPC SRS entries.

        :param fields: list of field names
        :param sirql_conditions: list of sirql conditions
        :return: RealDictCursor to the entities
        """
        return self._get_by_sirql(
            config.SWPC_SRS_TABLE, fields, sirql_conditions
        )

    def get_swpc_event_entries_by_sirql(
        self, fields=["*"], sirql_conditions=[]
    ):
        """
        Method which gets a list of SWPC EVENT entries.

        :param fields: list of field names
        :param sirql_conditions: list of sirql conditions
        :return: RealDictCursor to the entities
        """
        return self._get_by_sirql(
            config.SWPC_EVENT_TABLE, fields, sirql_conditions
        )

    def get_gevloc_entries_by_sirql(
        self, fields=["*"], sirql_conditions=[]
    ):
        """
        Method which gets a list of GEVLOC entries.

        :param fields: list of field names
        :param sirql_conditions: list of sirql conditions
        :return: RealDictCursor to the entities
        """
        return self._get_by_sirql(
            config.GEVLOC_TABLE, fields, sirql_conditions
        )

    def _get_by_sirql(self, table, fields=["*"], sirql_conditions=[]):
        """
        Method which gets a list of entities.

        :param table: the table to get entities from
        :param fields: list of field names
        :param sirql_conditions: list of sirql conditions
        :return: RealDictCursor to the entities
        """
        where = "True"
        if len(sirql_conditions) > 0:
            where = SiRQLToSql.convert(sirql_conditions)

        self.__cur.execute(
            "SELECT " + ",".join("%s" for f in fields) + " " +
            "FROM " + table + " " +
            "WHERE %s",
            (
                tuple([AsIs(f) for f in fields]) +
                (AsIs(where),)
            )
        )
        return self.__cur.fetchall()

    # delete
    def _delete(self, table, where={}):
        """
        Method which deletes entities.

        :param table: the table to delete entities from
        :param where: dictionary of conditions
        :return: Number of deleted rows
        """
        self.__cur.execute(
            "DELETE FROM " + table + " " +
            "WHERE True " + "".join("AND %s=%s " for k in where.keys()),
            (
                sum([(AsIs(key), where[key]) for key in where.keys()], ())
            )
        )
        return self.__cur.rowcount

    # delete by sirql
    def _delete_by_sirql(self, table, sirql_conditions=[]):
        """
        Method which deletes entities.

        :param table: the table to delete entities from
        :param sirql_conditions: list of sirql conditions
        :return: Number of deleted rows
        """
        where = "True"
        if len(sirql_conditions) > 0:
            where = SiRQLToSql.convert(sirql_conditions)

        self.__cur.execute(
            "DELETE FROM " + table + " " +
            "WHERE %s",
            (
                AsIs(where),
            )
        )
        return self.__cur.rowcount

    # update
    def _update(self, table, set={}, where={}):
        """
        Method which updates entities.

        :param table: the table to update entities from
        :param set: dictionary of assignments
        :param where: dictionary of conditions
        :return: Number of updated rows
        """
        if len(set) == 0:
            return 0
        self.__cur.execute(
            "UPDATE " + table + " " +
            "SET " + ",".join("%s=%s" for k in set.keys()) + " " +
            "WHERE True " + "".join("AND %s=%s " for k in where.keys()),
            (
                sum([(AsIs(key), set[key]) for key in set.keys()], ()) +
                sum([(AsIs(key), where[key]) for key in where.keys()], ())
            )
        )
        return self.__cur.rowcount
