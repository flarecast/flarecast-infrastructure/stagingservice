from datetime import timedelta
from flask import request
import iso8601

from stagingservice.data.staging_dao import StagingDAO

from stagingservice import sirql_util
import flarecast.service.return_handling


# --- get ---
def list():
    sirql_conditions = []

    for param in request.args:
        if (
            param == 'time_start' or
            param == 'time_peak' or
            param == 'time_end'
        ):
            sirql_conditions.append(
                *sirql_util.create_sirql_conditions_by_datetime(
                    param, request.args[param]
                )
            )
        else:
            sirql_conditions.append(
                *sirql_util.create_sirql_conditions_by_string(
                    param, request.args[param]
                )
            )

    # print map(SiRQLToSql.convert, [sirql_conditions])

    gevloc_entries = __list(sirql_conditions)
    return flarecast.service.return_handling.create_ok_response(gevloc_entries)


def __list(sirql_conditions=[]):
    with StagingDAO() as ctx:
        gevloc_entries = ctx.get_gevloc_entries_by_sirql(
            ['*'], sirql_conditions
        )

    for entry in gevloc_entries:
        if entry['time_start'] is not None:
            entry['time_start'] = entry['time_start'].isoformat()
        if entry['time_peak'] is not None:
            entry['time_peak'] = entry['time_peak'].isoformat()
        if entry['time_end'] is not None:
            entry['time_end'] = entry['time_end'].isoformat()

    return gevloc_entries
