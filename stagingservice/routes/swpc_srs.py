from flask import request

from stagingservice.data.staging_dao import StagingDAO

from stagingservice import sirql_util
import flarecast.service.return_handling


# --- get ---
def list():
    sirql_conditions = []

    for param in request.args:
        if (param == 'file_creation'):
            sirql_conditions.append(
                *sirql_util.create_sirql_conditions_by_datetime(
                    param, request.args[param]
                )
            )
        else:
            sirql_conditions.append(
                *sirql_util.create_sirql_conditions_by_string(
                    param, request.args[param]
                )
            )

    # print map(SiRQLToSql.convert, [sirql_conditions])

    swpc_srs = __list(sirql_conditions)
    return flarecast.service.return_handling.create_ok_response(swpc_srs)


def __list(sirql_conditions=[]):
    with StagingDAO() as ctx:
        swpc_srs_entries = ctx.get_swpc_srs_entries_by_sirql(
            ['*'], sirql_conditions
        )

    for entry in swpc_srs_entries:
        if entry['timestamp'] is not None:
            entry['timestamp'] = entry['timestamp'].isoformat()
        if entry['file_creation'] is not None:
            entry['file_creation'] = entry['file_creation'].isoformat()
        if entry['data_received'] is not None:
            entry['data_received'] = entry['data_received'].isoformat()

    return swpc_srs_entries
