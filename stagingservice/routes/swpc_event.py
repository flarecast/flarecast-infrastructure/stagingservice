from datetime import timedelta
import flask
import iso8601

from stagingservice.data.staging_dao import StagingDAO

from stagingservice import sirql_util
import flarecast.service.return_handling


# --- get ---
def list():
    sirql_conditions = []

    for param in flask.request.args:
        if (
            param == 'file_creation' or
            param == 'time_start' or
            param == 'time_peak' or
            param == 'time_end'
        ):
            sirql_conditions.append(
                *sirql_util.create_sirql_conditions_by_datetime(
                    param, flask.request.args[param]
                )
            )
        else:
            sirql_conditions.append(
                *sirql_util.create_sirql_conditions_by_string(
                    param, flask.request.args[param]
                )
            )

    # print map(SiRQLToSql.convert, [sirql_conditions])

    swpc_events = __list(sirql_conditions)
    return flarecast.service.return_handling.create_ok_response(swpc_events)


def __list(sirql_conditions=[]):
    with StagingDAO() as ctx:
        swpc_event_entries = ctx.get_swpc_event_entries_by_sirql(
            ['*'], sirql_conditions
        )

    for entry in swpc_event_entries:
        if entry['timestamp'] is not None:
            entry['timestamp'] = entry['timestamp'].isoformat()
        if entry['file_creation'] is not None:
            entry['file_creation'] = entry['file_creation'].isoformat()
        if entry['time_start'] is not None:
            entry['time_start'] = entry['time_start'].isoformat()
        if entry['time_peak'] is not None:
            entry['time_peak'] = entry['time_peak'].isoformat()
        if entry['time_end'] is not None:
            entry['time_end'] = entry['time_end'].isoformat()

    return swpc_event_entries
