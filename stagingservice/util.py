__author__ = 'cansik'


def format_insert_output(output_data):
    return map(lambda i: 'ok', output_data)


def format_get_output(output_rows):
    result = map(format_single_output, output_rows)
    return result


def format_single_output(output_row):
    if output_row is None:
        return None
    record = output_row.get_record()
    result = {k: v for k, v in record.iteritems() if k != 'id'}
    return result


def format_query_output(output_rows):
    if output_rows is None:
        return []
    return output_rows
