import iso8601
import re
import six
from sirql.SiRQLUtil import SiRQLUtil
from sirql.SiRQLParser import SiRQLParser


# create functions
def create_sirql_conditions_by_string(field, sirql_condition_string):
    return map(
        convert_sirql_condition_params_to_string,
        SiRQLParser.parse({
            field:
            fix_missing_sirql_condition_operator_to_eq(
                sirql_condition_string
            )
        })
    )


def create_sirql_conditions_by_datetime(field, sirql_condition_string):
    return map(
        convert_sirql_condition_params_to_string,
        map(
            convert_sirql_condition_params_to_datetime,
            SiRQLParser.parse({
                field:
                fix_missing_sirql_condition_operator_to_eq(
                    sirql_condition_string
                )
            })
        )
    )


# converter functions
def convert_sirql_condition_params_to_datetime(sirql_condition):
    sirql_condition.parameter = [
        iso8601.parse_date(param) for param in sirql_condition.parameter
    ]
    return sirql_condition


def convert_sirql_condition_params_to_string(sirql_condition):
    sirql_condition.parameter = [
        SiRQLUtil.as_string(
            param
        ) for param in sirql_condition.parameter if not isinstance(
            param, six.string_types
        ) or not (
            param.startswith("'") and param.endswith("'")
        )
    ]
    return sirql_condition


# preprocessing functions
def fix_missing_sirql_condition_operator_to_eq(sirql_condition_string):
    match = re.compile(
        ur'(?P<not>not_)?(?P<operator>\w+)\('
    ).search(sirql_condition_string)
    if match is None:
        sirql_condition_string = "eq(%s)" % (sirql_condition_string)
    return sirql_condition_string
