#!/usr/bin/env bash

title='Flarecast Staging Service Test'
echo -n -e "\033]0;$title\007"

# Run parallel
echo "running parallel..."
uwsgi --http :8005 -w main -p 16
