#!/usr/bin/python
from docker import Client
from docker.utils import kwargs_from_env

kwargs = kwargs_from_env()
if kwargs:
    docker_client = Client(**kwargs_from_env(assert_hostname=False))
else:
    docker_client = Client(base_url='unix://var/run/docker.sock')

containers = docker_client.containers(
    all=True, filters={"label": "eu.flarecast.testing=1"}
)

for i in containers:
    docker_client.stop(i)
    docker_client.remove_container(i, 2)
