import unittest

from base_test_case import BaseTestCase, TEST_SWPC_SRS_ENTRIES, \
    TEST_SWPC_EVENT_ENTRIES, TEST_GEVLOC_ENTRIES
from util import post_request, get_request, delete_request

__author__ = 'cansik'


class SwpcSrsTests(BaseTestCase):
    __GET_SWPC_SRS_ENTRIES = '/swpc_srs/list'

    @classmethod
    def setUpClass(self):
        super(SwpcSrsTests, self).setUpClass()

    def test_1_get_swpc_srs_entries(self):
        result = get_request(
            (self.service_url + self.__GET_SWPC_SRS_ENTRIES)
        )
        self.assertEqual(2, len(result["data"]))

        result = get_request(
            (self.service_url + self.__GET_SWPC_SRS_ENTRIES + "?nar=%s") %
            (TEST_SWPC_SRS_ENTRIES[0]["nar"])
        )
        self.assertEqual(1, len(result["data"]))

    @classmethod
    def tearDownClass(self):
        super(SwpcSrsTests, self).tearDownClass()


if __name__ == '__main__':
    unittest.main()
