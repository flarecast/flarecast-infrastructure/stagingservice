import unittest

from base_test_case import BaseTestCase, TEST_SWPC_SRS_ENTRIES, \
    TEST_SWPC_EVENT_ENTRIES, TEST_GEVLOC_ENTRIES
from util import post_request, get_request, delete_request

__author__ = 'cansik'


class GevlocTests(BaseTestCase):
    __GET_GEVLOC_ENTRIES = '/gevloc/list'

    @classmethod
    def setUpClass(self):
        super(GevlocTests, self).setUpClass()

    def test_1_get_swpc_event_entries(self):
        result = get_request(
            (self.service_url + self.__GET_GEVLOC_ENTRIES)
        )
        self.assertEqual(2, len(result["data"]))

        result = get_request(
            (self.service_url + self.__GET_GEVLOC_ENTRIES + "?ename=%s") %
            (TEST_GEVLOC_ENTRIES[0]["ename"])
        )
        self.assertEqual(1, len(result["data"]))

        result = get_request(
            (self.service_url + self.__GET_GEVLOC_ENTRIES +
             "?time_start=gt(%s)&time_end=lt(%s)") %
            (TEST_GEVLOC_ENTRIES[0]["time_start"],
             TEST_GEVLOC_ENTRIES[0]["time_end"])
        )
        self.assertEqual(1, len(result["data"]))

    @classmethod
    def tearDownClass(self):
        super(GevlocTests, self).tearDownClass()


if __name__ == '__main__':
    unittest.main()
