import requests

__author__ = 'cansik'


def post_request(url, payload):
    headers = {'Content-Type': 'application/json'}
    r = requests.post(url, json=payload, headers=headers)

    if r.status_code != 200:
        raise Exception(r)

    return r.json()


def delete_request(url):
    r = requests.delete(url)

    if r.status_code != 200:
        raise Exception(r)

    return r.json()


def get_request(url, params={}):
    r = requests.get(url, params=params)

    if r.status_code != 200:
        raise Exception(r)

    return r.json()
