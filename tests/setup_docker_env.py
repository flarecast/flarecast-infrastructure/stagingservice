#!/usr/bin/python
import time
import traceback

from docker import Client
from docker.utils import kwargs_from_env

links = {
    "db": "db",
}
port_bindings = {
    8005: 8005
}

try:
    kwargs = kwargs_from_env()
    if kwargs:
        docker_client = Client(**kwargs_from_env(assert_hostname=False))
    else:
        docker_client = Client(base_url='unix://var/run/docker.sock')

    # setup hmi_service-stub container
    dbservice = docker_client.create_container(
        image="flarecast/postgres",
        name="db",
        ports=[5432,
               5432],
        host_config=docker_client.create_host_config(
            port_bindings={5432: 5432}
        ),
        detach=True,
        labels={"eu.flarecast.testing": "1"}
    )["Id"]
    docker_client.start(dbservice)
    time.sleep(10)
    stagingservice = docker_client.create_container(
        image="flarecast/stagingservice",
        detach=True,
        name="staging",
        ports=[8005,
               8005],
        host_config=docker_client.create_host_config(
            links=links, port_bindings=port_bindings
        ),
        labels={"eu.flarecast.testing": "1"}
    )["Id"]
    docker_client.start(stagingservice)
    time.sleep(10)
except Exception as e:
    print(traceback.format_exc())
    docker_client.stop(dbservice)
    docker_client.remove_container(dbservice, 2)
    docker_client.stop(stagingservice)
    docker_client.remove_container(stagingservice, 2)
    raise (e)
