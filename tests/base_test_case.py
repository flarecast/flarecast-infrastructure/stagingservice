import psycopg2
import sys
import unittest

__author__ = 'cansik'


# global definitions
TEST_SERVICE_URL = 'http://%s:%s'

# test data
TEST_SWPC_SRS_ENTRIES = [
  {
    "id": 1,
    "nar": 2472,
    "lat_hg": 4,
    "long_hg": -11
  },
  {
    "id": 2,
    "nar": 2473,
    "lat_hg": -22,
    "long_hg": -9
  }
]

TEST_SWPC_EVENT_ENTRIES = [
  {
    "id": 1,
    "event": 8350,
    "time_start": '2015-03-20T02:05:59Z',
    "time_end": '2015-03-20T06:05:01Z',
    "observatory": 'LEA',
    "type": 'LPS',
    "long_hg": None,
    "long_hg": None,
    "regnum": 2297
  },
  {
    "id": 2,
    "event": 3660,
    "time_start": '2015-04-22T03:19:59Z',
    "time_end": '2015-04-22T08:22:01Z',
    "observatory": 'LEA',
    "type": 'LPS',
    "long_hg": None,
    "long_hg": None,
    "regnum": 2322
  }
]

TEST_GEVLOC_ENTRIES = [
  {
    "id": 1,
    "time_start": '2002-09-26T11:39:59Z',
    "time_end": '2002-09-26T13:00:01Z',
    "nar": 0,
    "long_hg": 19,
    "long_hg": 47,
    "ename": 'gev_20020926_1140'
  },
  {
    "id": 2,
    "time_start": '2002-09-27T14:31:59Z',
    "time_end": '2002-09-27T15:10:01Z',
    "nar": 0,
    "long_hg": 13,
    "long_hg": -40,
    "ename": 'gev_20020927_1432'
  }
]


class BaseTestCase(unittest.TestCase):
    service_url = TEST_SERVICE_URL % ('localhost', 8005)

    def setUp(self):
        # as we do not yet support insert routes for the
        # staging service we manually add some data here
        __con = psycopg2.connect(
            host="db",
            database='flarecast_data',
            port=5432,
            user='postgres',
            password=''
        )
        __cur = __con.cursor()

        # insert some SWPC SRS data
        __cur.execute(
            ("INSERT INTO flarecast_staging.swpc_srs (" +
             "    timestamp, file_creation, data_received, nar, lat_hg," +
             " long_hg, long_carr," +
             "    area, mcint_zur, mcint_pen, mcint_com," +
             "    dlong_hg, n_spots, mtwil_class" +
             ") VALUES (" +
             "    %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s" +
             ") RETURNING id"),
            ('2016-09-19 15:49:27.914149+00', '2016-09-19 15:49:27.914149+00',
             '2016-09-19 15:49:27.914149+00', 2472, 4, -11,
             329, 30, 'C', 's', 'o', '08', 7, 'BETA')
        )
        __cur.execute(
            ("INSERT INTO flarecast_staging.swpc_srs (" +
             "    timestamp, file_creation, data_received, nar," +
             " lat_hg, long_hg, long_carr," +
             "    area, mcint_zur, mcint_pen, mcint_com," +
             "    dlong_hg, n_spots, mtwil_class" +
             ") VALUES (" +
             "    %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s" +
             ") RETURNING id"),
            ('2016-09-19 15:49:27.914149+00', '2016-09-19 15:49:27.914149+00',
             '2016-09-19 15:49:27.914149+00', 2473, 4, -11,
             331, 590, 'F', 'k', 'c', '16', 36, 'BETA-GAMMA-DELTA')
        )

        # insert some SWPC EVENT data
        __cur.execute(
            ("INSERT INTO flarecast_staging.swpc_event (" +
             "    timestamp, file_creation, event, multi_event_flag," +
             " time_start," +
             "    time_peak, time_end, observatory, quality, type," +
             "    lat_hg, long_hg, frequency, wavelength," +
             "    particulars_1, particulars_2, particulars_3, regnum" +
             ") VALUES (" +
             "    %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s," +
             "    %s, %s, %s, %s, %s" +
             ") RETURNING id"),
            ('2016-09-19 15:53:41.020668+00', '2016-09-19 15:49:27.914149+00',
             8350, 'f',
             '2015-03-20 02:06:00+00', None, '2015-03-20 06:05:00+00',
             'LEA', 4, 'LPS', None, None, '', '', '0.1', '', '', 2297)
        )
        __cur.execute(
            ("INSERT INTO flarecast_staging.swpc_event (" +
             "    timestamp, file_creation, event, multi_event_flag," +
             "time_start," +
             "    time_peak, time_end, observatory, quality, type," +
             "    lat_hg, long_hg, frequency, wavelength," +
             "    particulars_1, particulars_2, particulars_3, regnum" +
             ") VALUES (" +
             "    %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s," +
             "    %s, %s, %s, %s, %s" +
             ") RETURNING id"),
            ('2016-09-19 15:53:41.020668+00', '2016-09-19 15:49:27.914149+00',
             3660, 'f',
             '2015-04-22 03:20:00+00', None, '2015-04-22 08:22:00+00',
             'LEA', 3, 'LPS', None, None, '', '', '0.08', '', '', 2322)
        )

        # insert some GEVLOC data
        __cur.execute(
            ("INSERT INTO flarecast_staging.gevloc (" +
             "    time_start, time_end, time_peak, xray_class," +
             "    lat_hg, long_hg, long_carr, nar, ename" +
             ") VALUES (" +
             "    %s, %s, %s, %s, %s, %s, %s, %s, %s" +
             ") RETURNING id"),
            ('2002-09-26 11:40:00+00', '2002-09-26 13:00:00+00',
             '2002-09-26 12:22:00+00', 'C1.7', 19, 47, 181.56, 0,
             'gev_20020926_1140')
        )
        __cur.execute(
            ("INSERT INTO flarecast_staging.gevloc (" +
             "    time_start, time_end, time_peak, xray_class," +
             "    lat_hg, long_hg, long_carr, nar, ename" +
             ") VALUES (" +
             "    %s, %s, %s, %s, %s, %s, %s, %s, %s" +
             ") RETURNING id"),
            ('2002-09-27 14:32:00+00', '2002-09-27 15:10:00+00',
             '2002-09-27 14:46:00+00', 'C1.6', 13, -40, 79.78, 0,
             'gev_20020927_1432')
        )

        __con.commit()

        __cur.close()
        __con.close()
        pass

    def tearDown(self):
        # clean up tables after testing
        __con = psycopg2.connect(
            host="db",
            database='flarecast_data',
            port=5432,
            user='postgres',
            password=''
        )
        __cur = __con.cursor()

        __cur.execute(
            "DELETE FROM flarecast_staging.swpc_srs"
        )
        __cur.execute(
            "DELETE FROM flarecast_staging.swpc_event"
        )
        __cur.execute(
            "DELETE FROM flarecast_staging.gevloc"
        )

        __con.commit()

        __cur.close()
        __con.close()
        pass
